package com.app.parq_me_up

import android.app.NotificationManager
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.ui.home.ActiveTransactionActivity
import com.app.parq_me_up.ui.login.AccessViewModelFactory
import com.app.parq_me_up.ui.login.LoginActivity
import com.app.parq_me_up.ui.login.LoginViewModel
import com.app.parq_me_up.ui.profile.NewCarActivity
import com.app.parq_me_up.utiles.ConnectivityUtilities

class MainActivity : AppCompatActivity() {


    private val connectivityReceiver = ConnectivityReceiver()
    private var connected: Boolean = false
    lateinit var loginViewModel: LoginViewModel
    private var notificationManager: NotificationManager? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProviders.of(this, AccessViewModelFactory())
            .get(LoginViewModel::class.java)
        setContentView(R.layout.activity_main)

        var extras = intent.extras

        // This means that activity was started from new transaction notification
        if (extras != null && extras.getString("transactionId") !== null) {
            val intent = Intent(this, ActiveTransactionActivity::class.java)
            intent.putExtra("isNew", true)
            intent.putExtra("transactionId", extras.getString("transactionId"))

            extras = null
            startActivity(intent)
        }


        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        //Register the connectivity receiver
        registerReceiver(
            connectivityReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        connected = ConnectivityUtilities.isConnected(this)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_transactions, R.id.navigation_profile
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(connectivityReceiver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_view, menu)
        menu.findItem(R.id.action_settings)
            ?.setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
                logout()
                return@OnMenuItemClickListener true
            })
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun logout() {
        loginViewModel.logout()
        val mainIntent = Intent(this, LoginActivity::class.java)
        finish()
        startActivity(mainIntent)
    }
}
