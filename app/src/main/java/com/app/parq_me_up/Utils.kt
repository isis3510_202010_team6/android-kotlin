package com.app.parq_me_up

class Utils {
    companion object {

        private var token : String = ""

        fun setToken(token : String){
            this.token = token
        }

        fun getToken(): String {
            return token;
        }
    }
}