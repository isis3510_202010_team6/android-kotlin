package com.app.parq_me_up.application

import android.app.Application

class ParqMeUp : Application(){
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener){
        ConnectivityReceiver.connectivityReceiverListener = listener
    }

    companion object{
        @get:Synchronized
        lateinit var instance : ParqMeUp
    }
}