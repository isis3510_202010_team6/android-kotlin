package com.app.parq_me_up.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity

class NotificationActivity: AppCompatActivity() {
    private var notificationManager: NotificationManager? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        notificationManager =
            getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun sendNotification(){

        createNotificationChannel(
            "request_notification",
            "Parking Request",
            "Parking Alert!"
        )

        val channelID = "request_notification"
        val notificationID = 101

        val notification = Notification.Builder(this, channelID)
            .setContentTitle("You have parked!")
            .setContentText("This is an example")
            .setSmallIcon(android.R.drawable.ic_menu_compass)
            .setChannelId(channelID)
            .build()
        notificationManager?.notify(notificationID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(id:String, name: String, description:String){
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(id, name, importance)

        channel.description = description
        channel.enableLights(true)
        channel.lightColor = Color.GREEN
        channel.enableVibration(false)
        channel.vibrationPattern = longArrayOf(100, 200, 300, 100, 100 ,100 )

        notificationManager?.createNotificationChannel(channel)


    }

}