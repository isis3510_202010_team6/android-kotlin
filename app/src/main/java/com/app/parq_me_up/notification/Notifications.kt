package com.app.parq_me_up.notification

import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi

class Notifications {

    @RequiresApi(Build.VERSION_CODES.O)
    fun sendNotification(view: View, context: Context, nm: NotificationManager){
        val channelID = "com.parqmeup.request_notification"
        val notificationID = 101

        val notification = Notification.Builder(context, channelID)
            .setContentTitle("You have parked!")
            .setContentText("This is an example")
            .setSmallIcon(android.R.drawable.ic_menu_compass)
            .setChannelId(channelID)
            .build()
        nm?.notify(notificationID, notification)
    }


}