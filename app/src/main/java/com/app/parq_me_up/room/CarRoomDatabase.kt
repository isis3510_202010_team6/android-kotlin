package com.app.parq_me_up.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.app.parq_me_up.ui.profile.Car
import com.app.parq_me_up.ui.profile.CarDao
import kotlinx.coroutines.CoroutineScope

@Database(entities = [Car::class], version = 1)
abstract class ParqRoomDatabase : RoomDatabase() {

    abstract fun carDao(): CarDao

    companion object {
        @Volatile
        private var INSTANCE: ParqRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): ParqRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        ParqRoomDatabase::class.java,
                        "car_database"
                    )
                    // Wipes and rebuilds instead of migrating if no Migration object.
                    // Migration is not part of this codelab.
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}