package com.app.parq_me_up.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.app.parq_me_up.ui.home.ParkingLot
import com.app.parq_me_up.ui.home.ParkingLotDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities=[ParkingLot::class], version=1)
abstract class ParkingLotRoomDatabase : RoomDatabase(){
    abstract fun parkingLotDao() : ParkingLotDao

    private class ParkingLotDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    var parkingLotDao = database.parkingLotDao()
                    // Delete all content here.
                    parkingLotDao.deleteAll()

                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE : ParkingLotRoomDatabase? = null

        fun getDataBase(
            context: Context,
            scope: CoroutineScope
        ): ParkingLotRoomDatabase {
            return INSTANCE
                ?: synchronized(this){
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        ParkingLotRoomDatabase::class.java,
                        "parkingLot_database"
                    )
                        .addCallback(ParkingLotDatabaseCallback(scope))
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                    instance
                }
        }


    }
}