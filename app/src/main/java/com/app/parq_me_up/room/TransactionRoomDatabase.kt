package com.app.parq_me_up.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.app.parq_me_up.ui.transactions.Transaction
import com.app.parq_me_up.ui.transactions.TransactionDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities=[Transaction::class], version=1)
abstract class TransactionRoomDatabase : RoomDatabase() {
    abstract fun transactionDao(): TransactionDao

    private class TransactionDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    var transactionDao = database.transactionDao()
                    // Delete all content here.
                    transactionDao.deleteAll()

                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE : TransactionRoomDatabase? = null

        fun getDataBase(
            context: Context,
            scope: CoroutineScope
        ): TransactionRoomDatabase {
            return INSTANCE
                ?: synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TransactionRoomDatabase::class.java,
                    "transaction_database"
                )
                    .addCallback(TransactionDatabaseCallback(scope))
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }


    }
}