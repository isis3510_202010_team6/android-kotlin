package com.app.parq_me_up.services

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.util.Log
import com.app.parq_me_up.MainActivity
import com.app.parq_me_up.ui.home.NewTransactionActivity
import com.app.parq_me_up.ui.profile.NewCarActivity
import com.app.parq_me_up.ui.profile.NewCardActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.concurrent.ExecutionException


class NotificationService : FirebaseMessagingService() {
    val TAG = "FIREBASE_MESSAGE"
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val notification = remoteMessage.notification
        if (notification != null) {
            val data = remoteMessage.data

            val messageType = remoteMessage.data.get("type")

            if (messageType == "NEW_TRANSACTION") {
                val intent = Intent(this, NewTransactionActivity::class.java)
                intent.putExtra("transactionId", remoteMessage.data.get(("transactionId")))
                intent.putExtra("parkingLotName", remoteMessage.data.get("parkingLotName"))
                intent.putExtra("plate", remoteMessage.data.get("plate"))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }
}