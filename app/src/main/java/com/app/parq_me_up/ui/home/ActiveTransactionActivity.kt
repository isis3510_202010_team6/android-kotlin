package com.app.parq_me_up.ui.home

import android.content.Intent
import android.icu.util.TimeUnit
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.MainActivity
import com.app.parq_me_up.R
import com.app.parq_me_up.Utils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_register.view.*
import org.json.JSONObject
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.floor

class ActiveTransactionActivity : AppCompatActivity() {

    var startTime: Date? = null
    val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.ms'Z'"

    lateinit var actualParking: TextView
    lateinit var actualAmount: TextView
    lateinit var actualCar: TextView
    lateinit var actualTime: TextView

    val handler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            val data = msg.obj as HashMap<String, String>

            if (data["minutes"] != null) {
                actualTime.text = data["minutes"]
            }
            if (data["cost"] != null) {
                actualAmount.text = data["cost"]
            }
            if (data["TransactionDone"] != null) {
                val intent = Intent(this@ActiveTransactionActivity, MainActivity::class.java)
                startActivity(intent)
            }
            if (data["startTime"] != null) {
                startTime = Date(data["startTime"]!!.toLong())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_active_home)

        val extras = intent.extras

        actualParking = findViewById<TextView>(R.id.parkinglot_name)
        actualAmount = findViewById<TextView>(R.id.active_amount)
        actualCar = findViewById<TextView>(R.id.active_car)
        actualTime = findViewById<TextView>(R.id.active_time)

        onFetchActive()

        val updateCostAndTimeThread = Thread() {
            kotlin.run {
                while (true) {
                    if (this.startTime != null) {
                        val currentDate = Calendar.getInstance().time
                        val diffSeconds =
                            java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(currentDate.time - this.startTime!!.time)
                        var cost = (diffSeconds * 300 / 60).toDouble()

                        var timeMinutes = (diffSeconds / 60).toDouble()

                        cost = floor(cost)

                        val map = HashMap<String, String>()
                        map.put("minutes", "%.2f".format(timeMinutes))
                        map.put("cost", cost.toString())
                        val msg = Message()
                        msg.obj = map
                        this.handler.sendMessage(msg)

                    }
                    Thread.sleep(1000)

                }
            }
        }
        updateCostAndTimeThread.start()

        val checkIfDoneThread = Thread() {
            kotlin.run {
                while (true) {
                    onFetchActive()
                    Thread.sleep(10000)
                }
            }
        }
        checkIfDoneThread.start()
    }


    private fun onFetchActive() {
        val TAG = "Transaction Fetch"

        val queue = Volley.newRequestQueue(this)
        val url = "${Constants.BASE_URL}/transactions/active"

        val request =
            object : StringRequest(Request.Method.GET, url, Response.Listener { res: String ->
                //Parse JSON
                val json = JSONObject(res)
                if (json.isNull("transaction")) {
                    val map = HashMap<String, String>()
                    map.put("TransactionDone", "true")
                    val msg = Message()
                    msg.obj = map
                    this.handler.sendMessage(msg)
                } else {
                    val transaction = json.getJSONObject("transaction")
                    val park = transaction.getJSONObject("parkingLot")
                    actualParking.text = park.getString("name")
                    actualCar.text = transaction.getString("vehicle_plate")

                    val startTransactionTime =
                        Date(SimpleDateFormat(dateFormat).parse(transaction.getString("startTime")).time - 5 * 3600 * 1000)

                    val map = HashMap<String, String>()
                    map.put("startTime", startTransactionTime.time.toString())
                    val msg = Message()
                    msg.obj = map
                    this.handler.sendMessage(msg)
                }

            }, Response.ErrorListener { error ->
                Log.i("error", error.localizedMessage)
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers
                }
            }

        queue.add(request)
    }
}