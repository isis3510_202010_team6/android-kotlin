package com.app.parq_me_up.ui.home

import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.R
import com.app.parq_me_up.Utils
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.notification.NotificationActivity
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import org.json.JSONObject

class HomeFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    ConnectivityReceiver.ConnectivityReceiverListener, SharedPreferences.OnSharedPreferenceChangeListener {

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }


    private lateinit var homeViewModel: HomeViewModel
    private lateinit var map: GoogleMap
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var rootLayout: ConstraintLayout
    private var snackBar: Snackbar? = null
    private lateinit var offlineText: TextView
    private lateinit var recyclerView : RecyclerView
    private lateinit var backButton : FloatingActionButton
    private lateinit var parkingLotsList : FloatingActionButton
    private var action : Int = 0
    private var nm = NotificationActivity()

    //--------------------------------------------------Foreground location service
    private var foregroundOnlyLocationServiceBound = false

    // Provides location updates for while-in-use feature.
    private var foregroundOnlyLocationService: ForegroundOnlyLocationService? = null

    // Listens for location broadcasts from ForegroundOnlyLocationService.
    private lateinit var foregroundOnlyBroadcastReceiver: ForegroundOnlyBroadcastReceiver

    private lateinit var sharedPreferences: SharedPreferences

    // Monitors connection to the while-in-use service.
    private val foregroundOnlyServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as ForegroundOnlyLocationService.LocalBinder
            foregroundOnlyLocationService = binder.service
            foregroundOnlyLocationServiceBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            foregroundOnlyLocationService = null
            foregroundOnlyLocationServiceBound = false
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        //UI elements reference
        var root = inflater.inflate(R.layout.fragment_home, container, false)
        rootLayout = root.findViewById(R.id.homeLayout)
        offlineText = root.findViewById(R.id.home_text_offline)

        foregroundOnlyBroadcastReceiver = ForegroundOnlyBroadcastReceiver()

        sharedPreferences =
            this.requireActivity().getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        foregroundOnlyLocationService?.subscribeToLocationUpdates()

        onFetchActive()

        //----------------- START Parking lot list management
        val context = context ?: return root
        val adapter = ParkingLotListAdapter(context)
        recyclerView = root.findViewById<RecyclerView>(R.id.recycler_view_parkingLots)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        //----------------- Parking lot list management

        parkingLotsList = root.findViewById<FloatingActionButton>(R.id.listParkingLotsButton)
        backButton = root.findViewById<FloatingActionButton>(R.id.backButton)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = childFragmentManager
            .findFragmentById(R.id.parking_lots_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        onFetchActive()

        if (ConnectivityUtilities.isConnected(requireActivity())) {
            offlineText.visibility = View.GONE
            parkingLotsList.setOnClickListener() {
                mapFragment.requireView().visibility = View.GONE
                parkingLotsList.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                backButton.visibility = View.VISIBLE

            }

            backButton.setOnClickListener {
                recyclerView.visibility = View.GONE
                backButton.visibility = View.GONE
                mapFragment.requireView().visibility = View.VISIBLE
                parkingLotsList.visibility = View.VISIBLE

            }

        } else {
            offlineText.visibility = View.VISIBLE
            mapFragment.requireView().visibility = View.GONE
        }

        parkingLotsList = root.findViewById<FloatingActionButton>(R.id.listParkingLotsButton)
        backButton = root.findViewById<FloatingActionButton>(R.id.backButton)


        homeViewModel.parkingLots.observe(
            viewLifecycleOwner,
            Observer { transaction -> transaction?.let { adapter.setParkingLots(it) } })


        return root
    }

    override fun onMarkerClick(p0: Marker?) = false


    private fun onFetchActive() {
        val TAG = "Transaction Fetch"

        val queue = Volley.newRequestQueue(this.context)
        val url = "${Constants.BASE_URL}/transactions/active"

        val request =
            object : StringRequest(Request.Method.GET, url, Response.Listener { res: String ->
                //Parse JSON
                val json = JSONObject(res)
                Log.i("${TAG} RES,", json.getString("transaction"))
                onSuccess(json)

            }, Response.ErrorListener { error ->
                Log.i("error", error.message.toString())
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers
                }
            }

        queue.add(request)
    }

    fun onSuccess(res: JSONObject) {
//        Log.i("REASFASDF", res.getString("transaction"))
        if (res.getString("transaction") != "null") {
            val activeIntent = Intent(this.context, ActiveTransactionActivity::class.java)
            val json = res.getJSONObject("transaction")
            val park = json.getJSONObject("parkingLot")

            activeIntent.putExtra("parking_lot", park.getString("name"))
            activeIntent.putExtra("car", json.getString("vehicle_plate"))
            activeIntent.putExtra("time", json.getString("startTime"))
            startActivity(activeIntent)
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        val boglocat = ForegroundOnlyBroadcastReceiver().getLoct()
        Log.i("Location", boglocat.toString())

        val myPlace = LatLng(4.624335, -74.063644)  // this is bogota
        map.addMarker(MarkerOptions().position(myPlace).title("Bogotá D.C"))
        map.moveCamera(CameraUpdateFactory.newLatLng(myPlace))

        map.getUiSettings().setZoomControlsEnabled(true)
        map.setOnMarkerClickListener(this)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 12.0f))


        if (ActivityCompat.checkSelfPermission(
                this.requireContext()!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this.requireActivity()!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                HomeFragment.LOCATION_PERMISSION_REQUEST_CODE
            )
        }
        else{
            if (::map.isInitialized) {
                map.uiSettings.isZoomControlsEnabled = true
                map.setOnMarkerClickListener(this)
                paintMap()
                map.uiSettings.isZoomControlsEnabled = true
                map.isMyLocationEnabled = true
            }
        }

    }

    private fun paintMap() {

        fusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15f))
                map.isMyLocationEnabled = true
            }
        }

        val logTag = "ParkingLotRequest"

        val queue = Volley.newRequestQueue(this.context)
        val url = "${Constants.BASE_URL}/parking-lots"

        Log.i(logTag, url)

        val request =
            object : StringRequest(Request.Method.GET, url, Response.Listener { res: String ->
                // Parse JSON
                Log.i("ParkingLot Response:", res)
                val gson = Gson()

                val parkingLotRespose =
                    gson?.fromJson(res, Array<ParkingLotSerializer>::class.java).toList()

                //Convert to Room entity
                val parkingLots: List<ParkingLot> =
                    parkingLotRespose.map<ParkingLotSerializer, ParkingLot> { t ->
                        t.toParkingLot()
                    }

                parkingLots.forEach {
                    map.addMarker(
                        MarkerOptions()
                            .position(LatLng(it.longitud, it.latitud))
                            .title(it.name)
                    )
                    Log.i("MARKER", "marker ${it.name} added")
                }

                this.homeViewModel.syncLocal(parkingLots)


            }, Response.ErrorListener { error ->
                // No internet connection -> show message
                if (error is NoConnectionError) {
                    Toast.makeText(
                        this.context,
                        "Can't fetch parking because there is no internet connection",
                        Toast.LENGTH_LONG
                    ).show()
                    Log.i(logTag, "NO_INTERNET: ${error.message.toString()}")
                }
                if (error is AuthFailureError) {
                    // TODO redirect to login page

                }

            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers;
                }
            }

        queue.add(request)

    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                rootLayout,
                "You are offline",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                snackBar?.dismiss()
            }
            //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()
            //TODO show the parking lot cached list instead
            //offlineText.visibility = View.VISIBLE
            mapFragment.requireView().visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            backButton.visibility = View.GONE
            parkingLotsList.visibility = View.GONE

            Log.i("NO RED", "there is no internet connection")
        } else {
            snackBar?.dismiss()
            offlineText.visibility = View.GONE
            recyclerView.visibility = View.GONE
            mapFragment.requireView().visibility = View.VISIBLE
            parkingLotsList.visibility = View.VISIBLE
            paintMap()
            Log.i("RED", "Internet is connected")
        }
    }

    //Foreground  requiered actions --------------------
    override fun onStart() {
        super.onStart()

        val serviceIntent = Intent(this.requireContext(), ForegroundOnlyLocationService::class.java)
        this.requireActivity().bindService(serviceIntent, foregroundOnlyServiceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this.requireContext()).registerReceiver(
            foregroundOnlyBroadcastReceiver,
            IntentFilter(
                ForegroundOnlyLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST)
        )

        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this.requireContext()).unregisterReceiver(
            foregroundOnlyBroadcastReceiver
        )
        super.onPause()
    }

    override fun onStop() {
        if (foregroundOnlyLocationServiceBound) {
            this.requireActivity().unbindService(foregroundOnlyServiceConnection)
            foregroundOnlyLocationServiceBound = false
        }
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)

        super.onStop()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        TODO("Not yet implemented")
    }

    /**
     * Receiver for location broadcasts from [ForegroundOnlyLocationService].
     */
    private inner class ForegroundOnlyBroadcastReceiver : BroadcastReceiver() {
        lateinit var location: Location

        init{
            location = Location("service Provider")
            location.latitude = 4.624335;
            location.longitude = -74.063644;
        }

        override fun onReceive(context: Context, intent: Intent) {
            val location = intent.getParcelableExtra<Location>(
                ForegroundOnlyLocationService.EXTRA_LOCATION
            )
        }

        fun getLoct() : Location{
            return location

        }

    }

}