package com.app.parq_me_up.ui.home

import android.app.Application
import androidx.lifecycle.*
import com.app.parq_me_up.room.ParkingLotRoomDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository : ParkingLotRepository
    var parkingLots : LiveData<List<ParkingLot>>

    init{
        val parkingLotDao = ParkingLotRoomDatabase.getDataBase(application, viewModelScope).parkingLotDao()
        repository = ParkingLotRepository(parkingLotDao)

        parkingLots = repository.getParkingLots
    }

    fun insert(parkingLot: ParkingLot) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(parkingLot)
        parkingLots = repository.getParkingLots
    }

    fun syncLocal(updatedParkingLots : List<ParkingLot>) = viewModelScope.launch(Dispatchers.IO) {
        repository.syncLocal(updatedParkingLots)
        parkingLots = repository.getParkingLots
    }
}