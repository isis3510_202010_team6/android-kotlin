package com.app.parq_me_up.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.MainActivity
import com.app.parq_me_up.R
import com.app.parq_me_up.Utils
import org.json.JSONObject

class NewTransactionActivity : AppCompatActivity() {

    lateinit var buttonAccept: Button
    lateinit var buttonReject: Button
    lateinit var carTV: TextView
    lateinit var parkingLotTV: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_transaction)


        buttonAccept = findViewById(R.id.button_accept_transaction)
        buttonReject = findViewById(R.id.button_reject_transaction)
        carTV = findViewById(R.id.new_transaction_car)
        parkingLotTV = findViewById(R.id.new_transaction_parking_lot)

        val extras = intent.extras

        if (extras != null) {
            val transactionId = extras.getString("transactionId")
            val plate = extras.getString("plate")
            val parkingLot = extras.getString("parkingLotName")

            carTV.text = plate
            parkingLotTV.text = parkingLot

            if (transactionId != null) {
                buttonAccept.setOnClickListener {
                    respondToTransaction(transactionId, true)
                }

                buttonReject.setOnClickListener {
                    respondToTransaction(transactionId, false)
                }
            }
        }
    }

    private fun respondToTransaction(transactionId: String, accepted: Boolean) {
        val url = "${Constants.BASE_URL}/transactions/${transactionId}"
        val queue = Volley.newRequestQueue(this)

        var result: String
        if (accepted) {
            result = "ACTIVE"
        } else {
            result = "REJECTED"
        }

        val jsonObject = JSONObject()
        jsonObject.put("state", result)

        val request =
            object : JsonObjectRequest(Request.Method.PUT, url, jsonObject,
                Response.Listener { res: JSONObject ->

                    this.buttonAccept.visibility = View.INVISIBLE
                    this.buttonReject.visibility = View.INVISIBLE

                    var intent: Intent
                    if (accepted) {
                        intent = Intent(this, ActiveTransactionActivity::class.java)
                        startActivity(intent)
                    } else {
                        intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    }
                }, Response.ErrorListener { error ->
                    // No internet connection -> show message
                    if (error is NoConnectionError) {
                        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                    }
                }) {

                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers;
                }

            }

        queue.add(request)
    }

}