package com.app.parq_me_up.ui.home

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="parkingLot")
class ParkingLot(
    @PrimaryKey() val id: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name= "description") val description: String,
    @ColumnInfo(name = "latitud") val latitud: Double,
    @ColumnInfo(name = "longitud") val longitud: Double,
    @ColumnInfo(name= "address") val address: String
)