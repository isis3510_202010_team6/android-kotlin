package com.app.parq_me_up.ui.home

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ParkingLotDao {

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(parkingLot: ParkingLot)

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    @Query("SELECT * FROM `parkingLot`")
    fun get(): LiveData<List<ParkingLot>>

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    @Query("DELETE FROM `parkingLot` where 1=1")
    fun deleteAll()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(parkingLots: List<ParkingLot>)
}