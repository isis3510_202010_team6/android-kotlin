package com.app.parq_me_up.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.parq_me_up.R
import com.app.parq_me_up.application.ConnectivityReceiver
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

class ParkingLotFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener{
    private var  snackBar: Snackbar? = null
    private lateinit var rootLayout: ConstraintLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_parking_lot, container, false)
        val context = context ?: return view
        val adapter = ParkingLotListAdapter(context)

        //UI elements reference for offline message
        rootLayout = view.findViewById(R.id.parkingLotLayout)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_parkingLots)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(rootLayout, "You are offline", Snackbar.LENGTH_LONG) //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_LONG
            snackBar?.show()

            Log.i("NO RED", "there is no internet connection")
        } else {
            snackBar?.dismiss()
            Log.i("RED", "Internet is connected")
        }
    }

}