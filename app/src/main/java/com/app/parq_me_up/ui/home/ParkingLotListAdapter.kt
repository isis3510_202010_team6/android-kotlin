package com.app.parq_me_up.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.parq_me_up.R

class ParkingLotListAdapter internal constructor(context: Context):
    RecyclerView.Adapter<ParkingLotListAdapter.ParkingLotViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var parkingLots = emptyList<ParkingLot>()

    inner class ParkingLotViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val name: TextView
        val address: TextView

        init{
            name = itemView.findViewById(R.id.name_parking_lot)
            address = itemView.findViewById(R.id.parking_lot_address)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParkingLotListAdapter.ParkingLotViewHolder {
        val view: View
        view = inflater.inflate(R.layout.parkinglot_list_row, parent, false)
        return ParkingLotViewHolder(view)
    }

    override fun onBindViewHolder(holder: ParkingLotListAdapter.ParkingLotViewHolder, position: Int) {
        val current = parkingLots[position]
            holder.name.text = current.name
            holder.address.text = current.address
    }

    internal fun setParkingLots(parkingLots: List<ParkingLot>){
        this.parkingLots = parkingLots
        notifyDataSetChanged()
    }

    override fun getItemCount() = parkingLots.size
}