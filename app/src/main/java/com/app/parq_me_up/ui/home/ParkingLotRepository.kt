package com.app.parq_me_up.ui.home

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.app.parq_me_up.ui.transactions.Transaction

class ParkingLotRepository(private val parkingLotDao: ParkingLotDao) {

    val getParkingLots: LiveData<List<ParkingLot>> = parkingLotDao.get()

    @Suppress("RedundantSuspendModifier")
    suspend fun insert(parkingLot: ParkingLot) {
        parkingLotDao.insert(parkingLot = parkingLot)
    }

    @Suppress("RedundantSuspendModifier")
    suspend fun syncLocal(parkingLots: List<ParkingLot>) {
        parkingLotDao.deleteAll()
        parkingLotDao.insert(parkingLots)
    }
}