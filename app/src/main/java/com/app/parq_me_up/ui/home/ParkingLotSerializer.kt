package com.app.parq_me_up.ui.home

import java.io.FileDescriptor
import java.util.*

class ParkingLotSerializer(
    val name: String,
    val description: String,
    val longitud: Double,
    val latitud: Double,
    val address: String,
    val id: String

) {
    fun toParkingLot(): ParkingLot {
        return ParkingLot(
            UUID.randomUUID().toString(),
            this.name,
            this.description,
            this.latitud,
            this.longitud,
            this.address
        )
    }
}