package com.app.parq_me_up.ui.login

import android.util.Log
import com.app.parq_me_up.Utils
import com.app.parq_me_up.application.ParqMeUp
import com.google.gson.Gson
import java.io.File

class AccessRepository() {
    // in-memory cache of the loggedInUser object
    var user: User? = null
    private set

    val isLoggedIn: Boolean
        get() = user != null


    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        readUserFromStorage()
        Log.i("INIT-ACCESS-REPO---", (user==null).toString())

    }

    fun logout() {
        setLoggedInUser(null)
    }

    fun access(user: User?, onSuccess: () -> Unit,  onError: (msg:String) -> Unit){
        try{
            setLoggedInUser(user)
            onSuccess()
        }catch ( e : Throwable){
            Log.i("err", "no auth user")
            onError("Check your credentials")
        }
    }

    private fun setLoggedInUser(user: User?) {
        this.user = user
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        writeUserToStorage()
    }

    private fun readUserFromStorage(){
        try{
            var gson = Gson()
            val bufferedReader = File(ParqMeUp.instance.filesDir,"userInfo").bufferedReader()
            val inputString = bufferedReader.use { it.readText() }
            var usuario = gson.fromJson(inputString, User::class.java)
            user = usuario
            Utils.setToken(token = usuario.token)
        }catch (e: Exception){
            e.printStackTrace()
            user = null

        }
    }

    private fun writeUserToStorage(){
        try{
            var gson = Gson()
            //Initialize the File Writer and write into file
            val file=File( ParqMeUp.instance.filesDir, "userInfo")
            if(user!=null){
                //Convert the Json object to JsonString
                var jsonString:String = gson.toJson(user)
                file.writeText(jsonString)
            }else{
                file.delete()
            }
        }catch (e: Exception){
            e.printStackTrace()
        }

    }

}