package com.app.parq_me_up.ui.login

/**
 * if.garcia
 * Live data to check login/register status of the user in the app
 */
data class AccessResult(
    val success: LoggedInUserView? = null,
    val error: Int? = null
)