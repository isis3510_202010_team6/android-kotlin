package com.app.parq_me_up.ui.login

data class LoggedInUserView(
    val userData: User
)