package com.app.parq_me_up.ui.login

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.volley.NoConnectionError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.MainActivity
import com.app.parq_me_up.R
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.ui.home.HomeFragment
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.app.parq_me_up.utiles.GeneralUtilities
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import com.newrelic.agent.android.NewRelic;


class LoginActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener{

    private lateinit var loginViewModel: LoginViewModel
    private var  snackBar: Snackbar? = null
    private val connectivityReceiver = ConnectivityReceiver()
    private var connected:Boolean = false

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        registerReceiver(connectivityReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        connected = ConnectivityUtilities.isConnected(this)
        showNetworkMessage(connected)

        //New Relic dependency initialization
        NewRelic.withApplicationToken(
            "AA85a58eb4970f492ba33c2c41c8a7191b1b6c7a5c-NRMA"
        ).start(this.getApplication());

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val redirect = findViewById<Button>(R.id.button)
        val loading = findViewById<ProgressBar>(R.id.loading)

        //TODO: changed - OK
        //OK suggest it has already been completed
        loginViewModel = ViewModelProviders.of(this, AccessViewModelFactory())
            .get(LoginViewModel::class.java)

        if (ActivityCompat.checkSelfPermission(
                this!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                HomeFragment.LOCATION_PERMISSION_REQUEST_CODE
            )
        }

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid && !loginState.isLoading
            if(login.isEnabled){
                login.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            }
            else{
                login.setBackgroundColor(resources.getColor(R.color.colorDisable))
            }

            redirect.isEnabled = !loginState.isLoading
            if(loginState.isLoading) loading.visibility = View.VISIBLE
            else loading.visibility = View.GONE

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        //TODO this redirects when a user has already logged in
        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val accessResult = it ?: return@Observer
            if (accessResult.error != null) {
                showLoginFailed(accessResult.error)
            }
            if (accessResult.success != null) {
                Log.i("storedUser", accessResult.success.userData.name )
                onSuccess()
            }
            setResult(Activity.RESULT_OK)
        })

        username.doAfterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            doAfterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            login.setOnClickListener {

                    if(ConnectivityUtilities.isConnected(this.context!!)){

                        handleLogin(email = username.text.toString(), password = password.text.toString(),
                            onSuccess = ::onSuccess, onError = ::onError
                        )
                    }
                    else {
                        GeneralUtilities.errorDialog(this.context!!)
                        GeneralUtilities.errorDialog(this@LoginActivity)

                    }
            }

            redirect.setOnClickListener {
                registerRedirect()
            }
        }

    }

    private fun handleLogin(email : String, password : String, onSuccess: () -> Unit,  onError: (msg:String) -> Unit){
        val logTag = "LoginRequest"

        val queue = Volley.newRequestQueue(this)
        val url = "${Constants.BASE_URL}/users/login"

        Log.i(logTag, url)
        val jsonObject = JSONObject()
        jsonObject.put("email", email)
        jsonObject.put("password", password)
        jsonObject.put("fcmToken", FirebaseInstanceId.getInstance().getToken())

        val request =
            @RequiresApi(Build.VERSION_CODES.O)
            object : JsonObjectRequest(url, jsonObject,
                Response.Listener { res: JSONObject ->
                    val user = User(
                        name = res.getString("name"),
                        email = res.getString("email"),
                        phone = res.getString("phone"),
                        token = res.getString("token")
                    )

                    loginViewModel.login(userData = user, onSuccess = onSuccess, onError = onError)

                }, Response.ErrorListener { error ->
                // No internet connection -> show message
                    if (error is NoConnectionError) {
                        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                        Log.i(logTag, "NO_INTERNET: ${error.message.toString()}")
                    }
                    else{
                        onError("Check your credentials")
                    }
            }) {

            }

        queue.add(request)

    }

    private fun registerRedirect(){
        val regIntent = Intent(this, RegisterActivity::class.java)
        startActivity(regIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(connectivityReceiver)
    }

    private fun onSuccess(){
        val mainIntent = Intent(this, MainActivity::class.java)
        startActivity(mainIntent)
    }

    private fun onError(msg: String){
        GeneralUtilities.builDialog(this, "Authentication fail", msg).show()
        loading.visibility = View.GONE
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                findViewById(R.id.container),
                "You are offline",
                Snackbar.LENGTH_INDEFINITE).setAction("OK") {
                snackBar?.dismiss()
            }
            //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()

            Log.i("NO RED", "there is no internet connection")
        } else {
            snackBar?.dismiss()
            Log.i("RED", "Internet is connected")
        }
    }
}