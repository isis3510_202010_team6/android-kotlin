package com.app.parq_me_up.ui.login

/**
 * if.garcia
 * Live Data use to check user inputs in the UI
 */
data class LoginFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false,
    val isLoading: Boolean = false
)
