package com.app.parq_me_up.ui.login

import android.os.Build
import android.util.Log
import android.util.Patterns
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.app.parq_me_up.R

@RequiresApi(Build.VERSION_CODES.O)
class LoginViewModel(private val accessRepository: AccessRepository) : ViewModel(){


    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<AccessResult>()
    val loginResult: LiveData<AccessResult> = _loginResult

    init {
        if(accessRepository.isLoggedIn){
            _loginResult.value = AccessResult(
                success = LoggedInUserView( user()!!)
            )
            Log.i("LoginViewModel -", "bla bla bla")
        }
    }

    fun login(userData : User, onSuccess: () -> Unit,  onError: (msg:String) -> Unit) {
        _loginForm.value = LoginFormState(isLoading = true)
        accessRepository.access(userData, onSuccess, { msg:String ->
            onError(msg)
            _loginForm.value = LoginFormState(isLoading = false)
        })
    }

    fun user(): User?{
        return accessRepository.user
    }

    fun logout(){
        accessRepository.logout()
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
                    && username.length < 100
        } else {
            username.isEmpty()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 5 && password.isNotEmpty()
    }
}