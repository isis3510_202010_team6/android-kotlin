package com.app.parq_me_up.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.android.volley.NoConnectionError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.MainActivity
import com.app.parq_me_up.R
import com.app.parq_me_up.Utils
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.app.parq_me_up.utiles.GeneralUtilities
import com.google.android.material.internal.ContextUtils.getActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject

class RegisterActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {

    private lateinit var registerViewModel: RegisterViewModel
    private var snackBar: Snackbar? = null
    private var connected: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        connected = ConnectivityUtilities.isConnected(this)
        showNetworkMessage(connected)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val name = findViewById<EditText>(R.id.name)
        val numCel = findViewById<EditText>(R.id.numcel)
        val register = findViewById<Button>(R.id.register)
        val loading = findViewById<ProgressBar>(R.id.loadingRegister)
        val loginRedirect = findViewById<Button>(R.id.login_redirect)

        findViewById<EditText>(R.id.numcel).setInputType(InputType.TYPE_CLASS_NUMBER)

        loginRedirect.setOnClickListener { loginRedirect() }

        registerViewModel = ViewModelProviders.of(this, AccessViewModelFactory())
            .get(RegisterViewModel::class.java)

        registerViewModel.registerFormState.observe(this@RegisterActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            register.isEnabled = loginState.isDataValid && !loginState.isLoading
            loginRedirect.isEnabled = !loginState.isLoading
            if (register.isEnabled) {
                register.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            } else {
                register.setBackgroundColor(resources.getColor(R.color.colorDisable))
            }

            if (loginState.isLoading) loading.visibility = View.VISIBLE
            else loading.visibility = View.GONE


            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
            if (loginState.nameError != null) {
                name.error = getString(loginState.nameError)
            }
            if (loginState.cellphoneError != null) {
                numCel.error = getString(loginState.cellphoneError)
            }

            if (loginState.emptyName != null) {
                name.error = getString(loginState.emptyName)
            }
            if (loginState.emptyUsername != null) {
                username.error = getString(loginState.emptyUsername)
            }
            if (loginState.emptyCellphone != null) {
                numCel.error = getString(loginState.emptyCellphone)
            }
            if (loginState.emptyPassword != null) {
                password.error = getString(loginState.emptyPassword)
            }
        })

        registerViewModel.registerResult.observe(this@RegisterActivity, Observer {
            val registerResult = it ?: return@Observer

            if (registerResult.error != null) {
                showLoginFailed(registerResult.error)
            }
            if (registerResult.success != null) {

            }
            setResult(Activity.RESULT_OK)
        })

        username.doAfterTextChanged {
            registerViewModel.registerDataChanged(
                username.text.toString(),
                password.text.toString(),
                name.text.toString(),
                numCel.text.toString()
            )
        }

        password.doAfterTextChanged {
            registerViewModel.registerDataChanged(
                username.text.toString(),
                password.text.toString(),
                name.text.toString(),
                numCel.text.toString()
            )
        }
        name.doAfterTextChanged {
            registerViewModel.registerDataChanged(
                username.text.toString(),
                password.text.toString(),
                name.text.toString(),
                numCel.text.toString()
            )
        }
        numCel.doAfterTextChanged {
            registerViewModel.registerDataChanged(
                username.text.toString(),
                password.text.toString(),
                name.text.toString(),
                numCel.text.toString()
            )
        }

        register.setOnClickListener {

            if (ConnectivityUtilities.isConnected(this)) {
                handleRegister(
                    email = username.text.toString(),
                    password = password.text.toString(),
                    name = name.text.toString(),
                    cellphone = numCel.text.toString(), onSuccess = ::onSuccess, onError = ::onError
                )
            } else {
                GeneralUtilities.errorDialog(this@RegisterActivity)
            }
        }

    }

    private fun handleRegister(
        email: String,
        password: String,
        name: String,
        cellphone: String,
        onSuccess: () -> Unit,
        onError: (msg: String) -> Unit
    ) {
        val logTag = "RegisterRequest"

        val queue = Volley.newRequestQueue(this)
        val url = "${Constants.BASE_URL}/users"

        val token = FirebaseInstanceId.getInstance().getToken()
        Log.d("Firebase", "token " + token);

        Log.i(logTag, url)
        val jsonObject = JSONObject()
        jsonObject.put("email", email)
        jsonObject.put("password", password)
        jsonObject.put("name", name)
        jsonObject.put("phone", cellphone)
        jsonObject.put("fcmToken", token)

        val request =
            object : JsonObjectRequest(url, jsonObject,
                Response.Listener { res: JSONObject ->

                    Log.i("REGISTER - RES", "$res")
                    // Parse JSON
                    val user = User(
                        name = res.getString("name"),
                        email = res.getString("email"),
                        phone = res.get("phone").toString(),
                        token = res.getString("token")
                    )

                    registerViewModel.register(user, onSuccess, onError)

                }, Response.ErrorListener { error ->
                    Log.d("errorReg", error.message.toString());

                    // No internet connection -> show message
                    if (error is NoConnectionError) {
                        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                        Log.i(logTag, "NO_INTERNET: ${error.message.toString()}")
                    } else {
                        onError(error.message.toString())
                    }
                }) {

            }

        queue.add(request)

    }

    fun onSuccess() {
        val regIntent = Intent(this, MainActivity::class.java)
        startActivity(regIntent)
    }

    fun onError(msg: String) {
        //Toast.makeText(this, "Fallo la creacion de un usuario", Toast.LENGTH_LONG).show()
        GeneralUtilities.builDialog(this, "Fallo en el registro", msg).show()
    }

    private fun loginRedirect() {
        val regIntent = Intent(this, LoginActivity::class.java)
        startActivity(regIntent)
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }


    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        connected = isConnected
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                findViewById(R.id.register_container),
                "You are offline",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                snackBar?.dismiss()
            } //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()
        } else {
            snackBar?.dismiss()
        }
    }

}