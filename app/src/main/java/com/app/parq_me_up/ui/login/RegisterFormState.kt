package com.app.parq_me_up.ui.login

data class RegisterFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val nameError: Int? = null,
    val cellphoneError: Int? = null,
    val emptyUsername: Int? = null,
    val emptyName: Int? = null,
    val emptyPassword: Int? = null,
    val emptyCellphone: Int? = null,
    val isDataValid: Boolean = false,
    val isLoading: Boolean = false
    )