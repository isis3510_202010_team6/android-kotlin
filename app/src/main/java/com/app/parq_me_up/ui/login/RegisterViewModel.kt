package com.app.parq_me_up.ui.login

import android.app.Application
import android.util.Log
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.parq_me_up.R

class RegisterViewModel(private val accessRepository: AccessRepository) : ViewModel(){


    private val _registerForm = MutableLiveData<RegisterFormState>()
    val registerFormState: LiveData<RegisterFormState> = _registerForm

    private val _registerResult = MutableLiveData<AccessResult>()
    val registerResult: LiveData<AccessResult> = _registerResult

    init {
        Log.i("RegisterViewModel -", "bla bla bla")
    }


    fun register(user : User, onSuccess: () -> Unit,  onError: (msg:String) -> Unit) {
        _registerForm.value = RegisterFormState(isLoading = true)
        accessRepository.access(user, {
            onSuccess()
            _registerForm.value = RegisterFormState(isLoading = false)
        }, {
            onError(it)
            _registerForm.value = RegisterFormState(isLoading = false)
        })
    }

    fun registerDataChanged(username:String, password: String, name:String, numCel:String) {
        if (!isUserNameValid(username)) {
            _registerForm.value = RegisterFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _registerForm.value = RegisterFormState(passwordError = R.string.invalid_password)
        } else if (!isCellphoneValid(numCel)) {
            _registerForm.value = RegisterFormState(cellphoneError = R.string.invalid_cellphone)
        } else if (!isNameValid(name)) {
            _registerForm.value = RegisterFormState(nameError = R.string.invalid_cellphone)
        } else if(username.isEmpty()){
            _registerForm.value = RegisterFormState(emptyUsername = R.string.empty_field)
        } else if(password.isEmpty()){
            _registerForm.value = RegisterFormState(emptyPassword = R.string.empty_field)
        } else if(name.isEmpty()){
            _registerForm.value = RegisterFormState(emptyName = R.string.empty_field)
        } else if(numCel.isEmpty()){
            _registerForm.value = RegisterFormState(emptyCellphone = R.string.empty_field)
        }
        else {
            _registerForm.value = RegisterFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
                    && username.length < 100
        } else {
            username.isEmpty()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 10 && password.isNotEmpty()
    }

    // A placeholder cellphone validation check
    private fun isCellphoneValid(cellphone: String): Boolean {
        return cellphone.length == 10 && cellphone.isNotEmpty()
    }

    // A placeholder password validation check
    private fun isNameValid(name: String): Boolean {
        return name.length < 100
    }

}
