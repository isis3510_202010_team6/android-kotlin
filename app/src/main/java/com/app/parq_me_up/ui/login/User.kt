package com.app.parq_me_up.ui.login

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class User(
    var name: String,
    var email: String,
    var phone: String,
    var token: String
)