package com.app.parq_me_up.ui.profile

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "car")
data class Car(
    @PrimaryKey(autoGenerate = false) val id: String,
    @ColumnInfo(name = "license_plate") val licensePlate: String,
    @ColumnInfo(name = "brand") val brand: String,
    @ColumnInfo(name = "active") val active: Boolean
) {
}