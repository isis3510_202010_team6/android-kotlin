package com.app.parq_me_up.ui.profile

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

@Dao
interface CarDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(car: Car)

    @Query("SELECT * FROM car")
    fun get(): LiveData<List<Car>>

    @Delete
    fun delete(car: Car)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(car: List<Car>)

    @Query("DELETE FROM `car` where 1=1 ")
    fun deleteAll()

    @Query("UPDATE car SET active = 1 WHERE id =:carId")
    fun setActive(carId: String)

    @Query("UPDATE car SET active = 0 WHERE id = :carId")
    fun setInactive(carId: String)
}