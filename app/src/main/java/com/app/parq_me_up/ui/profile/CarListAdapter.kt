package com.app.parq_me_up.ui.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.parq_me_up.R


class CarListAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<CarListAdapter.CarViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var cars = emptyList<Car>()

    var deleteCarListener: ((Car) -> Unit)? = null
    var setActiveCarListener: ((Car, Boolean) -> Unit)? = null


    inner class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val licensePlate: TextView
        val brand: TextView
        val active: Switch
        val delete: ImageButton

        init {
            licensePlate = itemView.findViewById(R.id.text_license_plate)
            brand = itemView.findViewById(R.id.text_car_brand)
            active = itemView.findViewById(R.id.switch_activate_car)
            delete = itemView.findViewById(R.id.button_delete)

            delete.setOnClickListener {
                deleteCarListener?.invoke(cars[adapterPosition])
            }

            active.setOnCheckedChangeListener { _, isChecked ->
                setActiveCarListener?.invoke(cars[adapterPosition], isChecked)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val view: View
        view = inflater.inflate(R.layout.profile_car_list_row, parent, false)
        return CarViewHolder(view)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val current = cars[position]
        holder.licensePlate.text = current.licensePlate
        holder.brand.text = current.brand
        holder.active.setOnCheckedChangeListener(null);
        holder.active.isChecked = current.active
        holder.active.setOnCheckedChangeListener { _, isChecked ->
            setActiveCarListener?.invoke(current, isChecked)
        }
    }

    internal fun setCars(cars: List<Car>) {
        this.cars = cars
        notifyDataSetChanged()
    }

    override fun getItemCount() = cars.size


}

