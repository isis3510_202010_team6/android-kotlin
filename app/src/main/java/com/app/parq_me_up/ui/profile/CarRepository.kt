package com.app.parq_me_up.ui.profile

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class CarRepository(private val carDao: CarDao) {
    val getCars: LiveData<List<Car>> = carDao.get()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(car: Car) {
        carDao.insert(car)
    }

    @SuppressWarnings("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(car: Car) {
        carDao.delete(car)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun syncCars(cars: List<Car>){
        carDao.deleteAll()
        carDao.insert(cars)
    }

    @SuppressWarnings("RedundantSuspendModifier")
    @WorkerThread
    suspend fun setActive(car: Car, active: Boolean) {
        if (active)
            carDao.setActive(car.id)
        else
            carDao.setInactive(car.id)
    }
}