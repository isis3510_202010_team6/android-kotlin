package com.app.parq_me_up.ui.profile

import java.util.*

class CarSerializer(
    val plate: String,
    var id: String,
    val description: String,
    val state: String
){
    fun toCar(): Car{

        return if(this.state.equals("ACTIVE", true)){
            Car(this.id, this.plate, this.description, true)
        } else{
            Car(this.id, this.plate, this.description, false)
        }
    }
}