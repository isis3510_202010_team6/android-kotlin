package com.app.parq_me_up.ui.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.parq_me_up.R


class CardListAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<CardListAdapter.CardViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var cards = emptyList<PaymentMethod>()

    var deleteCardListener: ((PaymentMethod) -> Unit)? = null
    var setActiveCardListener: ((PaymentMethod, Boolean) -> Unit)? = null

    inner class CardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardNumber: TextView
        val active: Switch
        val delete: ImageButton

        init {
            cardNumber = itemView.findViewById(R.id.profile_card_number)
            active = itemView.findViewById(R.id.switch_active_card)
            delete = itemView.findViewById(R.id.button_delete)

            delete.setOnClickListener {
                deleteCardListener?.invoke(cards[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val view: View
        view = inflater.inflate(R.layout.payment_method_list_row, parent, false)
        return CardViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val current = cards[position]
        holder.cardNumber.text = "***" + current.cardNumber.substring(current.cardNumber.length-5, current.cardNumber.lastIndex)
    }

    fun setCards(cards: List<PaymentMethod>) {
        this.cards = cards
        notifyDataSetChanged()
    }

    override fun getItemCount() = cards.size


}

