package com.app.parq_me_up.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.app.parq_me_up.R
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.app.parq_me_up.utiles.GeneralUtilities
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_new_card.*

class NewCarActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private lateinit var newCarViewModel: NewCarViewModel
    private lateinit var inputLicensePlateLetters: EditText
    private lateinit var inputLicensePlateNumbers: EditText
    private lateinit var inputBrand: Spinner
    private var snackBar: Snackbar? = null
    private var brand = "Mazda"
    private val brands = arrayOf("Mazda", "Chevrolet", "Audi","BMW", "Ford", "Renault", "Kia", "Mercedes-Benz", "Toyota", "Peugeot")


    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        newCarViewModel = ViewModelProviders.of(this).get(NewCarViewModel::class.java)
        setContentView(R.layout.activity_new_car)

        inputLicensePlateLetters = findViewById(R.id.car_letters)
        inputLicensePlateNumbers = findViewById(R.id.car_numbers)
        inputBrand = findViewById(R.id.brands_spinner)

        inputBrand!!.onItemSelectedListener = this

        val arrAda = ArrayAdapter(this, android.R.layout.simple_spinner_item, brands)
        arrAda.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        inputBrand!!.adapter = arrAda

        val fab = findViewById<ImageButton>(R.id.new_car_button_add)
        fab.isEnabled = false
        fab.isClickable = false

        newCarViewModel.newCarFormState.observe(this@NewCarActivity, Observer{

            val newCarFormState = it ?: return@Observer
            fab.isEnabled = newCarFormState.isDataValid

            if(newCarFormState.licenseLettersError != null){
                inputLicensePlateLetters.error = getString(newCarFormState.licenseLettersError)
            }

            if(newCarFormState.licenseNumbersError != null){
                inputLicensePlateNumbers.error = getString(newCarFormState.licenseNumbersError)
            }

            if(newCarFormState.emptyLicenseLetters != null) {
                inputLicensePlateLetters.error = getString(newCarFormState.emptyLicenseLetters)
            }

            if(newCarFormState.emptyLicenseNumbers != null) {
                inputLicensePlateNumbers.error = getString(newCarFormState.emptyLicenseNumbers)
            }

        })

        inputLicensePlateLetters.doAfterTextChanged {
            newCarViewModel.onNewCarDataChanged(
                inputLicensePlateLetters.text.toString(),
                inputLicensePlateNumbers.text.toString()
            )
        }

        inputLicensePlateNumbers.doAfterTextChanged {
            newCarViewModel.onNewCarDataChanged(
                inputLicensePlateLetters.text.toString(),
                inputLicensePlateNumbers.text.toString()
            )
        }

        fab.setOnClickListener {

            if (ConnectivityUtilities.isConnected(this) && newCarViewModel.newCarFormState.value!!.isDataValid) {
                val intent = Intent()

                val licensePlate = inputLicensePlateLetters.text.toString().toUpperCase() + inputLicensePlateNumbers.text.toString()

                intent.putExtra("license_plate", licensePlate)
                intent.putExtra("brand", brand)

                setResult(Activity.RESULT_OK, intent)
                finish()

            }
            else if(!newCarViewModel.newCarFormState.value!!.isDataValid){
                GeneralUtilities.builDialog(this@NewCarActivity,
                    "Form input error",
                    "Check your new car info, there is something wrong")
                    .setPositiveButton(android.R.string.yes) { dialog, which ->
                    dialog.dismiss()
                }.show()
            }
            else {
                GeneralUtilities.errorDialog(this@NewCarActivity)
            }
        }

    }

    override fun onItemSelected(parent: AdapterView<*>, arg1: View, position: Int, id: Long) {
        brand = brands[position]
    }

    override fun onNothingSelected(parent: AdapterView<*>?){ }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                findViewById(R.id.new_car_root_layout),
                "You are offline",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                snackBar?.dismiss()
            } //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()
        } else {
            snackBar?.dismiss()
        }
    }
}