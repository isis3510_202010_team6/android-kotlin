package com.app.parq_me_up.ui.profile

data class NewCarFormState(
    val licenseLettersError: Int? = null,
    val licenseNumbersError: Int? = null,
    val emptyLicenseLetters: Int? = null,
    val emptyLicenseNumbers: Int? = null,
    val isDataValid: Boolean = false
)