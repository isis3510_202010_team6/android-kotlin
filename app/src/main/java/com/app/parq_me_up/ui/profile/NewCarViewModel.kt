package com.app.parq_me_up.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.parq_me_up.R

class NewCarViewModel: ViewModel() {

    private val _newCarForm = MutableLiveData<NewCarFormState>()
    val newCarFormState: LiveData<NewCarFormState> = _newCarForm

    fun onNewCarDataChanged(licenseLetters: String, licenseNumbers: String){
        if(!isLicenseLetterValid(licenseLetters)){
            _newCarForm.value = NewCarFormState( licenseLettersError = R.string.invalid_license_letters)
        }
        else if (!isLicenseNumberValid(licenseNumbers)) {
            _newCarForm.value = NewCarFormState( licenseNumbersError = R.string.invalid_license_numbers)
        }
        else if(licenseLetters.isEmpty()){
            _newCarForm.value = NewCarFormState( emptyLicenseLetters = R.string.empty_field)
        }
        else if(licenseNumbers.isEmpty()){
            _newCarForm.value = NewCarFormState( emptyLicenseNumbers = R.string.empty_field)
        }
        else{
            _newCarForm.value = NewCarFormState(isDataValid = true)
        }
    }

    private fun isLicenseLetterValid(licenseLetters: String): Boolean{
        if(licenseLetters.length != 3){
            return false
        } else{
            var charArr = licenseLetters.toCharArray()
            for( c in charArr){
                if(c.isDigit()) return false
            }
            return true
        }
    }

    private fun isLicenseNumberValid(licenseNumbers: String): Boolean{
        if(licenseNumbers.length != 3){
            return false
        } else {
            var charArr = licenseNumbers.toCharArray()
            for( c in charArr){
                if(!c.isDigit()) return false
            }
            return true
        }
    }

}