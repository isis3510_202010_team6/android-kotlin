package com.app.parq_me_up.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.app.parq_me_up.R
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.app.parq_me_up.utiles.GeneralUtilities
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_new_card.*
import kotlinx.android.synthetic.main.activity_register.view.*

class NewCardActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private lateinit var newCardViewModel: NewCardViewModel
    private lateinit var inputCardNumber: EditText
    private lateinit var inputSecurityCode: EditText
    private lateinit var inputExpDate_month: Spinner
    private lateinit var inputExpDate_year: Spinner
    private lateinit var inputName: EditText
    private lateinit var inputLastname: EditText
    private var snackBar: Snackbar? = null
    private val months = arrayOf("January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December")
    private val years = arrayOf("2020", "2021", "2022", "2023", "2024", "2025", "2026","2027", "2028", "2029", "2030")
    private var month = "January"
    private var year = "2021"

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        newCardViewModel = ViewModelProviders.of(this).get(NewCardViewModel::class.java)
        setContentView(R.layout.activity_new_card)

        inputName = findViewById(R.id.new_card_input_name)
        inputLastname = findViewById(R.id.new_card_input_lastname)
        inputCardNumber = findViewById(R.id.new_card_input_card_number)
        inputSecurityCode = findViewById(R.id.new_card_input_security_code)
        inputExpDate_month = findViewById(R.id.months_spinner)
        inputExpDate_year = findViewById(R.id.years_spinner)

        inputExpDate_month!!.onItemSelectedListener = this
        inputExpDate_year!!.onItemSelectedListener = this

        val arrAda = ArrayAdapter(this, android.R.layout.simple_spinner_item, months)
        arrAda.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        inputExpDate_month!!.adapter = arrAda

        val arrAdb = ArrayAdapter(this, android.R.layout.simple_spinner_item, years)
        arrAdb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        inputExpDate_year!!.adapter = arrAdb

        val fab = findViewById<FloatingActionButton>(R.id.new_card_save)
        fab.isEnabled = false
        fab.isClickable = false

        newCardViewModel.newCardFormState.observe(this@NewCardActivity, Observer{
            val newCardFormState = it ?: return@Observer
            fab.isEnabled = newCardFormState.isDataValid

            if(newCardFormState.emptyName != null){
                inputName.error = getString(newCardFormState.emptyName)
            }

            if(newCardFormState.emptyLastName != null){
                inputLastname.error = getString(newCardFormState.emptyLastName)
            }

            if(newCardFormState.emptyCardNumber != null) {
                inputCardNumber.error = getString(newCardFormState.emptyCardNumber)
            }

            if(newCardFormState.emptyCVV != null) {
                inputSecurityCode.error = getString(newCardFormState.emptyCVV)
            }
            if(newCardFormState.cardNumberError != null) {
                inputCardNumber.error = getString(newCardFormState.cardNumberError)
            }

            if(newCardFormState.cvvError != null) {
                inputSecurityCode.error = getString(newCardFormState.cvvError)
            }
        })

        inputName.doAfterTextChanged {
            newCardViewModel.onNewCardDataChanged(
                inputName.text.toString(),
                inputLastname.text.toString(),
                inputCardNumber.text.toString(),
                inputSecurityCode.text.toString()
            )
        }

        inputLastname.doAfterTextChanged {
            newCardViewModel.onNewCardDataChanged(
                inputName.text.toString(),
                inputLastname.text.toString(),
                inputCardNumber.text.toString(),
                inputSecurityCode.text.toString()
            )
        }

        inputCardNumber.doAfterTextChanged {
            newCardViewModel.onNewCardDataChanged(
                inputName.text.toString(),
                inputLastname.text.toString(),
                inputCardNumber.text.toString(),
                inputSecurityCode.text.toString()
            )
        }

        inputSecurityCode.doAfterTextChanged {
            newCardViewModel.onNewCardDataChanged(
                inputName.text.toString(),
                inputLastname.text.toString(),
                inputCardNumber.text.toString(),
                inputSecurityCode.text.toString()
            )
        }

        fab.setOnClickListener {

            if (ConnectivityUtilities.isConnected(this) && newCardViewModel.newCardFormState.value!!.isDataValid) {
                val intent = Intent()

                val cardNumber = inputCardNumber.text.toString()
                val securityCode = inputSecurityCode.text.toString()
                val expDateMonth = inputExpDate_month.toString()
                val expDateYear = inputExpDate_year.toString()
                val name = inputName.text.toString()
                val lastname = inputLastname.text.toString()

                intent.putExtra("cardNumber", cardNumber)
                intent.putExtra("securityCode", securityCode)
                intent.putExtra("expDateMonth", expDateMonth)
                intent.putExtra("expDateYear", expDateYear)
                intent.putExtra("name", name)
                intent.putExtra("lastname", lastname)

                setResult(Activity.RESULT_OK, intent)
                finish()
            }
            else if(!newCardViewModel.newCardFormState.value!!.isDataValid){
                GeneralUtilities.builDialog(this@NewCardActivity,
                    "Form input error",
                    "Check your new card info, there is something wrong")
                    .setPositiveButton(android.R.string.yes) { dialog, which ->
                    dialog.dismiss()
                }.show()
            }
            else{
                GeneralUtilities.errorDialog(this@NewCardActivity)
            }
        }

    }

    override fun onItemSelected(parent: AdapterView<*>, arg1: View, position: Int, id: Long) {
        if(parent.id == months_spinner.id){
           month = months[position]
        }
        else if(parent.id == years_spinner.id){
            year = years[position]
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?){ }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                findViewById(R.id.new_card_root_layout),
                "You are offline",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                snackBar?.dismiss()
            } //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()
        } else {
            snackBar?.dismiss()
        }
    }
}