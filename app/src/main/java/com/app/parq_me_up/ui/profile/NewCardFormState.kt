package com.app.parq_me_up.ui.profile

class NewCardFormState (
    val cardNumberError: Int? = null,
    val cvvError: Int? = null,
    val emptyName: Int? = null,
    val emptyLastName: Int? = null,
    val emptyCardNumber: Int? = null,
    val emptyCVV: Int? = null,
    val isDataValid: Boolean = false
)