package com.app.parq_me_up.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.parq_me_up.R

class NewCardViewModel: ViewModel() {

    private val _newCardForm = MutableLiveData<NewCardFormState>()
    val newCardFormState: LiveData<NewCardFormState> = _newCardForm

    fun onNewCardDataChanged(name: String, lastName: String, cardNumber : String, cvv : String){
        if(!isCardNumberValid(cardNumber)){
            _newCardForm.value = NewCardFormState( cardNumberError = R.string.invalid_card_number)
        }
        else if (!isCvvValid(cvv)) {
            _newCardForm.value = NewCardFormState( cvvError = R.string.invalid_cvv)
        }
        else if(cardNumber.isEmpty()){
            _newCardForm.value = NewCardFormState( emptyCardNumber = R.string.empty_field)
        }
        else if(cvv.isEmpty()){
            _newCardForm.value = NewCardFormState( emptyCVV = R.string.empty_field)
        }
        else if(name.isEmpty()){
            _newCardForm.value = NewCardFormState( emptyName = R.string.empty_field)
        }
        else if(lastName.isEmpty()){
            _newCardForm.value = NewCardFormState( emptyLastName = R.string.empty_field)
        }
        else{
            _newCardForm.value = NewCardFormState(isDataValid = true)
        }
    }

    private fun isCardNumberValid(cardNumber: String): Boolean{
        if(cardNumber.length != 16){
            return false
        } else{
            var charArr = cardNumber.toCharArray()
            for( c in charArr){
                if(!c.isDigit()) return false
            }
            return true
        }
    }

    private fun isCvvValid(cvv: String): Boolean{
        if(!(cvv.length == 2 || cvv.length == 3)){
            return false
        } else {
            var charArr = cvv.toCharArray()
            for( c in charArr){
                if(!c.isDigit()) return false
            }
            return true
        }
    }

}