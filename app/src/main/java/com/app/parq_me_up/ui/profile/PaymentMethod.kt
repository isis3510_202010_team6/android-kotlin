package com.app.parq_me_up.ui.profile

data class PaymentMethod(
    val id: String,
    val cardNumber: String,
    val securityCode: String,
    val expDate: String,
    val name: String,
    val lastName: String
) {
    override fun equals(other: Any?): Boolean {
        if (other is PaymentMethod) {
            return id == other.id
        } else {
            return false
        }
    }
}


