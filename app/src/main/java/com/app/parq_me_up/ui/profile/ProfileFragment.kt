package com.app.parq_me_up.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.R
import com.app.parq_me_up.Utils
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.ui.login.AccessRepository
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.app.parq_me_up.utiles.GeneralUtilities
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap


class ProfileFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener {

    private val newCarActivityRequestCode = 1;
    private val newCardActivityRequestCode = 2;

    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var cardAdapter: CardListAdapter
    private lateinit var userName: TextView
    private lateinit var userEmail: TextView
    private lateinit var userPhone: TextView
    private var snackBar: Snackbar? = null
    private lateinit var rootLayout: ConstraintLayout

    private lateinit var accessRepository: AccessRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        val context = context ?: return view

        //UI components
        rootLayout = view.findViewById(R.id.profileLayout)
        accessRepository = AccessRepository()

        // User Info
        userName = view.findViewById(R.id.profile_user_name)
        userEmail = view.findViewById(R.id.profile_email)
        userPhone = view.findViewById(R.id.profile_phone)
        getCurrentUser()

        // Car List
        val adapter = CarListAdapter(context)
        adapter.deleteCarListener = ::deleteCar;
        adapter.setActiveCarListener = ::toggleActiveCar;

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_cars)
        recyclerView.adapter = adapter;
        recyclerView.layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)


        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        profileViewModel.cars.observe(
            viewLifecycleOwner,
            Observer { cars -> cars?.let { adapter.setCars(it) } })

        // Add car
        val addCarButton = view.findViewById<ImageButton>(R.id.profile_add_car)
        addCarButton.setOnClickListener {
            if (ConnectivityUtilities.isConnected(requireActivity())) {
                val intent = Intent(getContext(), NewCarActivity::class.java)
                startActivityForResult(intent, newCarActivityRequestCode)
            } else {
                GeneralUtilities.errorDialog(requireActivity())
            }

        }

        // Card List
        cardAdapter = CardListAdapter(context)
        val cardRecyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_cards)
        cardRecyclerView.adapter = cardAdapter
        cardRecyclerView.layoutManager = GridLayoutManager(context, 2)

        cardAdapter.setCards(profileViewModel.cards)
        cardAdapter.deleteCardListener = ::deleteCard;
        cardAdapter.setActiveCardListener = ::toggleActiveCard;

        // Add card
        val addCardButton = view.findViewById<ImageButton>(R.id.profile_add_card)
        addCardButton.setOnClickListener {
            if (ConnectivityUtilities.isConnected(requireActivity())) {
                val intent = Intent(getContext(), NewCardActivity::class.java)
                startActivityForResult(intent, newCardActivityRequestCode)
            } else {
                GeneralUtilities.errorDialog(requireActivity())
            }
        }
        fetchCars()
        return view;
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //activity
        if (requestCode == newCarActivityRequestCode && resultCode == Activity.RESULT_OK) {
            Log.i("new car", "receive new car result")
            data?.let { data ->
                Log.i("new car", data.getStringExtra("license_plate"))
                Log.i("new car", data.getStringExtra("brand"))

                val car =
                    Car(
                        UUID.randomUUID().toString(),
                        data.getStringExtra("license_plate"),
                        data.getStringExtra("brand"),
                        active = false
                    )
                putCar(car)
                Log.i("new car", car.toString())
                profileViewModel.insert(car)
                Unit
            }
        } else if (requestCode == newCardActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.let { data ->

                val card = PaymentMethod(
                    UUID.randomUUID().toString(),
                    data.getStringExtra("cardNumber"),
                    data.getStringExtra("securityCode"),
                    data.getStringExtra("expDateMonth") + " - " + data.getStringArrayExtra("expDateYear"),
                    data.getStringExtra("name"),
                    data.getStringExtra("lastname")
                )
                profileViewModel.addCard(card)
                cardAdapter.setCards(profileViewModel.cards)
            }
        }
    }

    private fun setActiveCar(car: Car, active: Boolean) {
        val url = "${Constants.BASE_URL}/vehicles/active"

        val queue = Volley.newRequestQueue(this.context)

        val json = JSONObject()
        json.put("plate", car.licensePlate)
        json.put("inactive", active === false)

        val request =
            object : JsonObjectRequest(Request.Method.POST, url, json,
                Response.Listener { res: JSONObject ->
                    val response = res.toString()
                },
                Response.ErrorListener { err ->
                    Log.i("ERROR", "$err")
                }) {

                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers;
                }
            }
        queue.add(request)

    }

    private fun putCar(car: Car) {
        val url = "${Constants.BASE_URL}/vehicles"
        val queue = Volley.newRequestQueue(this.context)


        val json = JSONObject()
        json.put("plate", car.licensePlate)
        json.put("description", car.brand)

        val request =
            object : JsonObjectRequest(Request.Method.POST, url, json,
                Response.Listener { res: JSONObject ->
                    val response = res.toString()
                },
                Response.ErrorListener { err ->
                    Log.i("ERROR", "$err")
                }) {

                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers;
                }
            }
        queue.add(request)
    }

    private fun deleteCarApiCall(car: Car) {

        val queue = Volley.newRequestQueue(this.context)

        val url = "${Constants.BASE_URL}/vehicles/${car.licensePlate}"

        val request = object : StringRequest(Request.Method.DELETE, url, Response.Listener { res ->
            Log.i("Delete Response", res.toString())
        }, Response.ErrorListener { err ->
            Log.i("Delete error", err.toString())
        }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = Utils.getToken()
                return headers;
            }
        }

        queue.add(request)
    }

    private fun fetchCars() {
        val TAG = "Profile Fetch"

        val queue = Volley.newRequestQueue(this.context)
        val url = "${Constants.BASE_URL}/vehicles"

        val request =
            object : StringRequest(Request.Method.GET, url, Response.Listener { res: String ->
                //Parse JSON
                val gson = Gson()
                Log.i("${TAG} RES,", res)
                val response =
                    gson?.fromJson(res, Array<CarSerializer>::class.java).toList()

                val cars: List<Car> =
                    response.map<CarSerializer, Car> { t ->
                        t.id = UUID.randomUUID().toString()

                        t.toCar()
                    }

                this.profileViewModel.syncCars(cars);

            }, Response.ErrorListener { error ->
                Log.i("error", error.localizedMessage)
            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers
                }
            }

        queue.add(request)
    }

    fun deleteCar(car: Car) {
        deleteCarApiCall(car)
        profileViewModel.delete(car)
        Log.d("onClickCar", "CLICKED ON CAR $car")
    }

    fun toggleActiveCar(car: Car, active: Boolean) {
        setActiveCar(car, active)
        profileViewModel.setActive(car, active);
        Log.d("onClickCar", "SWITCHED ON CAR $car")
    }

    fun deleteCard(card: PaymentMethod) {
        profileViewModel.deleteCard(card)
        cardAdapter.notifyDataSetChanged()
    }

    fun toggleActiveCard(card: PaymentMethod, active: Boolean) {

    }

    fun getCurrentUser() {
        userName.text = accessRepository.user?.name
        userEmail.text = accessRepository.user?.email
        userPhone.text = accessRepository.user?.phone
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                rootLayout,
                "You are offline",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                snackBar?.dismiss()
            } //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()

            Log.i("NO RED", "there is no internet connection")
        } else {
            snackBar?.dismiss()
            Log.i("RED", "Internet is connected")
        }
    }

}