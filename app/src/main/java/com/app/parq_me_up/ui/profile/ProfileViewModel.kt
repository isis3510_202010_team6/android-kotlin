package com.app.parq_me_up.ui.profile

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.LiveData
import com.app.parq_me_up.room.ParqRoomDatabase
import com.app.parq_me_up.ui.login.AccessRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProfileViewModel(application: Application) : AndroidViewModel(application) {
    private val carRepository: CarRepository
    private val userRepository: AccessRepository

    var cars: LiveData<List<Car>>

    val cards = mutableListOf<PaymentMethod>()

    init {
        val carDao = ParqRoomDatabase.getDatabase(application, viewModelScope).carDao();
        carRepository = CarRepository(carDao)
        cars = carRepository.getCars

        userRepository = AccessRepository()

    }

    fun insert(car: Car) = viewModelScope.launch(Dispatchers.IO) {
        carRepository.insert(car)
    }

    fun addCard(card: PaymentMethod) {
        cards.add(card)
    }

    fun deleteCard(card: PaymentMethod) {
        cards.remove(card)
    }

    fun setActiveCard(card: PaymentMethod, active: Boolean) {

    }

    fun syncCars(updatedCars: List<Car>) = viewModelScope.launch(Dispatchers.IO) {
        carRepository.syncCars(updatedCars)
        cars = carRepository.getCars
    }


    fun delete(car: Car) = viewModelScope.launch(Dispatchers.IO) {
        carRepository.delete(car)
    }

    fun setActive(car: Car, active: Boolean) = viewModelScope.launch(Dispatchers.IO) {
        carRepository.setActive(car, active)
    }
}