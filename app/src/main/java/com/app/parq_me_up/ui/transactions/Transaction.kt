package com.app.parq_me_up.ui.transactions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="transaction")
data class Transaction(
    @PrimaryKey() val id: String,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name="parking_lot") val parking_lot: String,
    @ColumnInfo(name = "amount") val amount: Double,
    @ColumnInfo(name = "car_id") val license_plate: String,
//  En segundos
    @ColumnInfo(name="time_spent") val time_spent: Int,
    @ColumnInfo(name = "payment_method_id") val payment_method_id: Int,
    @ColumnInfo(name = "status") val status: String
)

