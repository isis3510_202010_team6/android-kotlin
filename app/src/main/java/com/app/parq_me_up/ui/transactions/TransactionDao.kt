package com.app.parq_me_up.ui.transactions

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TransactionDao {

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(transaction: Transaction)

    @WorkerThread
    @Query("SELECT * FROM `transaction`")
    fun get(): LiveData<List<Transaction>>

    @WorkerThread
    @Query("DELETE FROM `transaction` where 1=1")
    fun deleteAll()

    @WorkerThread
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(transaction: List<Transaction>)
}