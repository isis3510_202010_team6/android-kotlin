package com.app.parq_me_up.ui.transactions

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.parq_me_up.Constants
import com.app.parq_me_up.R
import com.app.parq_me_up.Utils
import com.app.parq_me_up.application.ConnectivityReceiver
import com.app.parq_me_up.utiles.ConnectivityUtilities
import com.app.parq_me_up.utiles.GeneralUtilities
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson


class TransactionFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener {
    private lateinit var transactionViewModel: TransactionViewModel
    private var  snackBar: Snackbar? = null
    private lateinit var rootLayout: ConstraintLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_transactions, container, false)
        val context = context ?: return view
        val adapter = TransactionListAdapter(context)

        //UI elements reference for offline message
        rootLayout = view.findViewById(R.id.transactionsLayout)


        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_transactions)
        recyclerView.adapter = adapter;
        recyclerView.layoutManager = LinearLayoutManager(context)

        transactionViewModel = ViewModelProvider(this).get(TransactionViewModel::class.java)

        transactionViewModel.transactions.observe(
            viewLifecycleOwner,
            Observer { transaction -> transaction?.let { adapter.setTransactions(it) } })

        fetchTransactions()

        return view
    }

    private fun fetchTransactions() {
        val logTag = "TransactionsRequest"

        val queue = Volley.newRequestQueue(this.context)
        val url = "${Constants.BASE_URL}/transactions"

        Log.i(logTag, url)

        val request =
            object : StringRequest(Request.Method.GET, url, Response.Listener { res: String ->
                // Parse JSON
                val gson = Gson()
                Log.i("Response ----:", res)
                val transactionResponse =
                    gson?.fromJson(res, Array<TransactionSerializer>::class.java).toList()


                // Convert to Room entity
                val transactions: List<Transaction> =
                    transactionResponse.map<TransactionSerializer, Transaction> { t ->
                        t.toTransaction()
                    }

                // Update cache
                this.transactionViewModel.syncLocal(transactions)

            }, Response.ErrorListener { error ->
                // No internet connection -> show message
                if (error is NoConnectionError) {
                    Toast.makeText(this.context, "Can't fetch transactions because there is no internet connection", Toast.LENGTH_LONG).show()
                    Log.i(logTag, "NO_INTERNET: ${error.message.toString()}")
                }
                if (error is AuthFailureError) {
                    // TODO redirect to login page

                }

            }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = Utils.getToken()
                    return headers;
                }
            }

        queue.add(request)

    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(rootLayout,
                "You are offline",
                Snackbar.LENGTH_INDEFINITE).setAction("OK") {
                snackBar?.dismiss()
            }
            //Assume "rootLayout" as the root layout of every activity.
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()

            Log.i("NO RED", "there is no internet connection")
        } else {
            snackBar?.dismiss()
            Log.i("RED", "Internet is connected")
        }
    }
}