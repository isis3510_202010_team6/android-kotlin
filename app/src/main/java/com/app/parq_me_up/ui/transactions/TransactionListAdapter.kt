package com.app.parq_me_up.ui.transactions

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.parq_me_up.R

class TransactionListAdapter internal constructor(context: Context):
    RecyclerView.Adapter<TransactionListAdapter.TransactionViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var transactions = emptyList<Transaction>()

    inner class TransactionViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val licensePlate: TextView
        val date: TextView
        val amount: TextView
        val parkingLot: TextView
        val time: TextView
//        val status: Spinner

        init{
            licensePlate = itemView.findViewById(R.id.text_license_plate)
            date = itemView.findViewById(R.id.text_date)
            amount = itemView.findViewById(R.id.text_amount)
            parkingLot = itemView.findViewById(R.id.text_parking_lot)
            time = itemView.findViewById(R.id.text_time)
//            status = itemView.findViewById(R.id.text_status)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view: View
        view = inflater.inflate(R.layout.transaction_list_row, parent, false)
        return TransactionViewHolder(view)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val current = transactions[position]
        holder.licensePlate.text = current.license_plate
        holder.amount.text = current.amount.toString()
        holder.date.text = current.date
        holder.parkingLot.text = current.parking_lot
        holder.time.text = current.time_spent.toString()
    }

    internal fun setTransactions(transactions: List<Transaction>){
        this.transactions = transactions
        notifyDataSetChanged()
    }

    override fun getItemCount() = transactions.size

}