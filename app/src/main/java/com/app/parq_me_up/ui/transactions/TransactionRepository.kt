package com.app.parq_me_up.ui.transactions

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.android.volley.toolbox.Volley

class TransactionRepository(private val transactionDao: TransactionDao) {

    val getTransaction: LiveData<List<Transaction>> = transactionDao.get()

    @Suppress("RedundantSuspendModifier")
    suspend fun insert(transaction: Transaction) {
        transactionDao.insert(transaction)
    }

    @Suppress("RedundantSuspendModifier")
    suspend fun syncLocal(transactions: List<Transaction>) {
        transactionDao.deleteAll()
        transactionDao.insert(transactions)
    }
}