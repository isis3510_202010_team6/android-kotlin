package com.app.parq_me_up.ui.transactions

import com.google.gson.JsonObject
import java.text.DateFormat
import java.util.*
import kotlin.math.abs

data class TransactionSerializer(
    val state: String,
    val vehicle_plate: String,
    val startTime: Date,
    val userId: String,
    val parking_lot_id: String,
    val parkingLot: JsonObject,
    val id: String,
    val endTime: Date,
    val cost: Double,
    val updatedAt: Date,
    val createdAt: Date
) {
    fun toTransaction(): Transaction {
        var time = 0
        if (this.endTime != null) {
            time = abs(this.endTime.time - this.startTime.time / 60000).toInt()

        }
        return Transaction(
            this.id,
            this.startTime.toString(),
            this.parkingLot.get("name").toString(),
            this.cost,
            this.vehicle_plate,
            time,
            1,
            this.state
        )
    }
}