package com.app.parq_me_up.ui.transactions

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.app.parq_me_up.room.TransactionRoomDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TransactionViewModel(application: Application):AndroidViewModel(application) {
    private val repository: TransactionRepository

    var transactions: LiveData<List<Transaction>>

    init {
        val transactionDao =  TransactionRoomDatabase.getDataBase(application, viewModelScope).transactionDao();
        repository = TransactionRepository(transactionDao)

        transactions = repository.getTransaction
    }

    fun insert(transaction: Transaction) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(transaction)
        transactions = repository.getTransaction
    }

    fun syncLocal(updatedTransactions: List<Transaction>) = viewModelScope.launch(Dispatchers.IO) {
        repository.syncLocal(updatedTransactions)
        transactions = repository.getTransaction
    }


}