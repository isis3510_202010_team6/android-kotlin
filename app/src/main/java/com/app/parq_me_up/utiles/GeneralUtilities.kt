package com.app.parq_me_up.utiles

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import androidx.core.app.ActivityCompat
import androidx.core.content.edit
import com.app.parq_me_up.R


object GeneralUtilities {

    const val KEY_FOREGROUND_ENABLED = "tracking_foreground_location"

    fun builDialog(context: Context, title: String, description: String): AlertDialog.Builder {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(description)
        return builder
    }


    fun errorDialog(context: Context,title: Int = R.string.title_dialog_error,
                    body: Int = R.string.body_dialog_error) {
        builDialog(context,
            context.resources.getString(title),
            context.resources.getString(body))
            .setPositiveButton(android.R.string.yes) { dialog, which ->
                dialog.dismiss()
            }.show()
    }

    private fun makeRequest(activity: Activity, manifestPermission: String, code: Int) {
        ActivityCompat.requestPermissions(activity,
            arrayOf(manifestPermission),
            code
        )
    }

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The [Context].
     */
    fun getLocationTrackingPref(context: Context): Boolean =
        context.getSharedPreferences(
            context.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
            .getBoolean(KEY_FOREGROUND_ENABLED, false)

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    fun saveLocationTrackingPref(context: Context, requestingLocationUpdates: Boolean) =
        context.getSharedPreferences(
            context.getString(R.string.preference_file_key),
            Context.MODE_PRIVATE).edit {
            putBoolean(KEY_FOREGROUND_ENABLED, requestingLocationUpdates)
        }
}